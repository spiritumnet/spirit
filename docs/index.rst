.. Spiritum.Net documentation master file, created by
   sphinx-quickstart on Mon Jan 26 01:08:01 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Spiritum.Net's documentation!
========================================

This is an awesome **Spiritum.Net Boilerplate**!!

With this code you can start a *complex* Django Project
very quickly, with just a few steps!

Some of the Spiritum.Net functionalities are:

- **different virtual environments** for developing, testing and production
- **Internationalization** and **localization** to support different languages
- Project structure
- **HTML5 Boilerplate**
- Template Inheritance
- Functional **tests**
- robots.txt and humans.txt configured

To start using the Boilerplate, check out the :doc:`requirements`
and next the :doc:`quick_start`.

Contents:

.. toctree::
   :maxdepth: 2

   requirements
   quick_start



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

